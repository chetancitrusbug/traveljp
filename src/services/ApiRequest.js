export const postRequest = async (apiUrl, body) => {
    try {
      const response = await fetch(apiUrl, {
        // credentials: "include",
        method: "POST",
        // headers: {
        //   Accept: "application/json",
        //   "Content-Type": "application/json"
        // },
        body: JSON.stringify(body)
      });
      const result = await response.text();
      const responseJson = JSON.parse(result);
      return responseJson;
    } catch (error) {
      throw error;
    }
  };
  
  export const getRequest = async apiUrl => {
    try {
      const response = await fetch(apiUrl, {
        // credentials: "include",
        method: "GET",
        // headers: {
        //   Accept: "application/json",
        //   "Content-Type": "application/json"
        // }
      });
      const result = await response.text();
      const responseJson = JSON.parse(result);
      return responseJson;
    } catch (error) {
      throw error;
    }
  };

  export const putRequest = async (apiUrl, body) => {
    try {
      const response = await fetch(apiUrl, {
        // credentials: "include",
        method: "PUT",
        // headers: {
        //   Accept: "application/json",
        //   "Content-Type": "application/json"
        // },
        body: JSON.stringify(body)
      });
      const result = await response.text();
      const responseJson = JSON.parse(result);
      return responseJson;
    } catch (error) {
      throw error;
    }
  };