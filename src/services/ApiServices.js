import ApiUrl from './ApiUrl';
import { postRequest, getRequest, putRequest } from './ApiRequest'; 

export const SignUpService = async (userData) => {
    try {
        const res = await postRequest(ApiUrl.SIGN_UP, userData);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getOccupation = async () => {
    try {
        const res = await getRequest(ApiUrl.GET_OCCUPATION);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getAgeGroup = async () => {
    try {
        const res = await getRequest(ApiUrl.GET_AGE_GROUP);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getAreasToFollow = async () => {
    try {
        const res = await getRequest(ApiUrl.GET_AREAS_TO_FOLLOW);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getUserToFollow = async () => {
    try {
        const res = await getRequest(ApiUrl.GET_USER_TO_FOLLOW);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getGroupToFollow = async () => {
    try {
        const res = await getRequest(ApiUrl.GET_GROUP_TO_FOLLOW);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getWhatToNext= async () => {
    try {
        const res = await getRequest(ApiUrl.GET_WHAT_TO_DO_NEXT);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getAreasToFollowSearch = async (search) => {
    try {
        const res = await getRequest(ApiUrl.GET_AREAS_TO_FOLLOW + '?search' + '=' + search);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getUserToFollowSearch = async (search) => {
    try {
        const res = await getRequest(ApiUrl.GET_USER_TO_FOLLOW + '?search' + '=' + search);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getGroupToFollowSearch = async (search) => {
    try {
        const res = await getRequest(ApiUrl.GET_GROUP_TO_FOLLOW + '?search' + '=' + search);
        return res;
    } catch(error) {
        throw error;
    }
}

export const facebookSignupService = async () => {
    try {
        const res = await postRequest(ApiUrl.FACEBOOK_LOGIN);
        return res;
    } catch(error) {
        throw error;
    }
}

export const googleSignupService = async () => {
    try {
        const res = await postRequest(ApiUrl.GOOGLE_LOGIN);
        return res;
    } catch(error) {
        throw error;
    }
}

export const simpleSignupService = async (userData) => {
    try {
        const res = await postRequest(ApiUrl.SIMPLE_LOGIN, userData);
        return res;
    } catch(error) {
        throw error;
    }
}

export const updatePasswordService = async (userId,userData) => {
    try {
        const res = await putRequest(ApiUrl.UPDATE_PASSWORD + userId, userData);
        return res;
    } catch(error) {
        throw error;
    }
}
