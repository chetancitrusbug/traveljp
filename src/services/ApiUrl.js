const LOCAL_API = 'http://13.235.225.118/dev/laravel/map/public/api/';
const STAGING_API = '';

const BASE_URL = LOCAL_API;
const ApiUrls = {
    SIGN_UP: BASE_URL + 'signUp',
    GET_OCCUPATION: BASE_URL + 'getOccupation',
    GET_AGE_GROUP: BASE_URL + 'getAgeGroup',
    GET_AREAS_TO_FOLLOW: BASE_URL + 'getAreasToFollow',
    GET_USER_TO_FOLLOW: BASE_URL + 'getUsersToFollow',
    GET_GROUP_TO_FOLLOW: BASE_URL + 'getGroupToFollow',
    GET_WHAT_TO_DO_NEXT: BASE_URL + 'getWhatToDoNext',
    GOOGLE_LOGIN: BASE_URL + 'users/auth/google_oauth2',
    FACEBOOK_LOGIN: BASE_URL + 'users/auth/facebook',
    SIMPLE_LOGIN: BASE_URL + 'users',
    UPDATE_PASSWORD: BASE_URL + 'users/',
}

export default ApiUrls;
