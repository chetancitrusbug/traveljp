export const area = 
    [
        {
            id:1,
            localName:"秋葉原",
            name:"Akihabara",
            image_url:require("../assets/images/areas/areas-01.jpg")

        },
        {
            id:2,
            localName:"浅草",
            name:"Asakusa",
            image_url:require("../assets/images/areas/areas-02.jpg")

        },
        {
            id:3,
            localName:"原宿",
            name:"Harajuku",
            image_url:require("../assets/images/areas/areas-03.jpg")

        },
        {
            id:4,
            localName:"渋谷区",
            name:"Shibuya",
            image_url:require("../assets/images/areas/areas-04.jpg")

        },
        {
            id:5,
            localName:"上野",
            name:"Ueno",
            image_url:require("../assets/images/areas/areas-05.jpg")

        },
        {
            id:6,
            localName:"浅草",
            name:"Asakusa",
            image_url:require("../assets/images/areas/areas-06.jpg")

        },
        {
            id:7,
            localName:"秋葉原",
            name:"Akihabara",
            image_url:require("../assets/images/areas/areas-07.jpg")

        },
        {
            id:8,
            localName:"原宿",
            name:"Harajuku",
            image_url:require("../assets/images/areas/areas-08.jpg")

        },
        {
            id:9,
            localName:"渋谷区",
            name:"Shibuya",
            image_url:require("../assets/images/areas/areas-09.jpg")

        },
        {
            id:10,
            localName:"上野",
            name:"Ueno",
            image_url:require("../assets/images/areas/areas-10.jpg")

        }
    ];

export const user = 
    [
        {
            id:1,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user1.jpg")
        },
        {
            id:2,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user2.jpg")
        },
        {
            id:3,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user3.jpg")
        },
        {
            id:4,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user4.jpg")
        },
        {
            id:5,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user5.jpg")
        },
        {
            id:6,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user6.jpg")
        },
        {
            id:7,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user7.jpg")
        },
        {
            id:8,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user8.jpg")
        },
        {
            id:9,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user9.jpg")
        },
        {
            id:10,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user10.jpg")
        },
        {
            id:11,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user5.jpg")
        },
        {
            id:12,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user9.jpg")
        },
        {
            id:13,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user2.jpg")
        },
        {
            id:14,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user3.jpg")
        },
        {
            id:15,
            name:"Sunny",
            email:"sunnymint@gmail.com",
            image_url:require("../assets/images/user/user1.jpg")
        }


    ];

    export const group = 
    [
        {
            id:1,
            localName:"秋葉原",
            name:"Akihabara",
            image_url:require("../assets/images/groups/groups01.jpg")

        },
        {
            id:2,
            localName:"浅草",
            name:"Asakusa",
            image_url:require("../assets/images/groups/groups02.jpg")

        },
        {
            id:3,
            localName:"原宿",
            name:"Harajuku",
            image_url:require("../assets/images/groups/groups03.jpg")

        },
        {
            id:4,
            localName:"渋谷区",
            name:"Shibuya",
            image_url:require("../assets/images/groups/groups04.jpg")

        },
        {
            id:5,
            localName:"上野",
            name:"Ueno",
            image_url:require("../assets/images/groups/groups05.jpg")

        },
        {
            id:6,
            localName:"浅草",
            name:"Asakusa",
            image_url:require("../assets/images/groups/groups06.jpg")

        },
        {
            id:7,
            localName:"秋葉原",
            name:"Akihabara",
            image_url:require("../assets/images/groups/groups07.jpg")

        },
        {
            id:8,
            localName:"原宿",
            name:"Harajuku",
            image_url:require("../assets/images/groups/groups08.jpg")

        },
        {
            id:9,
            localName:"渋谷区",
            name:"Shibuya",
            image_url:require("../assets/images/groups/groups09.jpg")

        },
        {
            id:10,
            localName:"上野",
            name:"Ueno",
            image_url:require("../assets/images/groups/groups10.jpg")

        },
        {
            id:11,
            localName:"浅草",
            name:"Asakusa",
            image_url:require("../assets/images/groups/groups11.jpg")

        },
        {
            id:12,
            localName:"秋葉原",
            name:"Akihabara",
            image_url:require("../assets/images/groups/groups12.jpg")

        },
        {
            id:13,
            localName:"原宿",
            name:"Harajuku",
            image_url:require("../assets/images/groups/groups13.jpg")

        },
        {
            id:14,
            localName:"渋谷区",
            name:"Shibuya",
            image_url:require("../assets/images/groups/groups14.jpg")

        },
        {
            id:15,
            localName:"上野",
            name:"Ueno",
            image_url:require("../assets/images/groups/groups15.jpg")

        }


    ]


