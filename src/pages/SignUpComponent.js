
import React from 'react';
import Header from '../subcomponent/Header'
import $ from "jquery";
import UserRegistrationSectionContainer from '../containers/subContainers/UserRegistrationSectionContainer'
import ChooseOccupationContainer from '../containers/subContainers/ChooseOccupationContainer'
import AgeGroupContainer from '../containers/subContainers/AgeGroupContainer'
import AreaToFollowContainer from '../containers/subContainers/AreaToFollowContainer'
import UserToFollowContainer from '../containers/subContainers/UsersToFollowContainer'
import GroupToFollowContainer from '../containers/subContainers/GroupToFollowContainer'
import PasswordSectionContainer from '../containers/subContainers/PasswordSectionContainer'
import WhatNextContainer from '../containers/subContainers/WhatNextContainer'

class SignUpComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: false,
        }
    }
    componentDidUpdate(){
        // $('html, body').animate({ scrollTop: $('.'+this.props.currentactive).offset().top - $("#header-set").height() }, 1000);
        $(window).scrollTop($('.'+this.props.currentactive).offset().top - $("#header-set").height() );
    }

    renderMiddleSection = () =>{
        
    }

    render() {
        return (
            <div id="wrapper" className="wrapper home-wrapper">
                <Header isLogin={this.state.isLogin} />
                <div className="main-middle-area">
                    <section className="common-section">
                        <div className="background-image-full"></div>
                        
                        {this.props.section.baseRegistration && <UserRegistrationSectionContainer />}
                        {this.props.section.chooseOccupation && <ChooseOccupationContainer />}
                        {this.props.section.ageGroup && <AgeGroupContainer />}
                        {this.props.section.areaToFollow && <AreaToFollowContainer />}
                        {this.props.section.usersToFollow && <UserToFollowContainer />}
                        {this.props.section.groupToFollow && <GroupToFollowContainer />}
                        {this.props.section.passwordsection && <PasswordSectionContainer />}
                        {this.props.section.whatNext && <WhatNextContainer />} 
                        
                    </section>
                </div>
            </div>
        )
    }
}

export default SignUpComponent;