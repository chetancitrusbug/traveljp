import React from 'react';
import Header from '../subcomponent/Header';

class PlannersComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: true,
        }
    }

    render() {
        return (
            <div id="wrapper" className="wrapper home-wrapper">
                <Header isLogin={this.state.isLogin} />
                <div className="main-middle-area">
                    <section className="common-section">
                        <div className="background-image-full"></div>
                    </section>
                </div>
            </div>
        )
    }
}

export default PlannersComponent;