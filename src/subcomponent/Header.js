import React from 'react';
import history from '../services/BrowserHistory';
import logo from "../assets/images/logo.png";
import logoWhite from '../assets/images/logo-white.svg';
import menuWhite from "../assets/images/icons/menu-white.svg";
import profile from '../assets/images/profile.png';
import { connect } from 'react-redux';
import { setUserSignUp,setActiveSection } from '../actionCreators/index';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isUserLogin: false,
            isOpenMyAccount: false,
            isOpenMyProfile: false,
            isMobileNavBarOpen: false,
            isOpenLanguage: false,
            en: 'English',
            jp: 'Japan',
            iconEnClass: 'flag flag-us',
            iconJpClass: 'flag flag-jp',
            selectedLang: 'en',
            currentProfileType: false,
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
        this.setState({ isUserLogin: this.props.isLogin})
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    setWrapperRef = (node) => {
        this.wrapperRef = node;
    }

    setWrapperRefLang = (node) => {
        this.wrapperRefLang = node;
    }

    setWrapperRefProfile = (node) => {
        this.wrapperRefProfile = node;
    }

    handleClickOutside = (event) => {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({ isOpenMyAccount: false })
        }
        if (this.wrapperRefLang && !this.wrapperRefLang.contains(event.target)) {
            this.setState({ isOpenLanguage: false })
        }
        if (this.wrapperRefProfile && !this.wrapperRefProfile.contains(event.target)) {
            this.setState({ isOpenMyProfile: false })
        }
    }


    handleMyAccount = () => {
        this.setState({ isOpenMyAccount: !this.state.isOpenMyAccount })
    }

    handleMyProfile = () => {
        this.setState({ isOpenMyProfile: !this.state.isOpenMyProfile });
    }

    handleMobileNavBar = () => {
        this.setState({ isMobileNavBarOpen: !this.state.isMobileNavBarOpen });
    }

    handleLanguage = () => {
        this.setState({ isOpenLanguage: !this.state.isOpenLanguage });
    }

    selectLang = (lang) => {
        if (lang === 'en') {
            this.setState({ selectedLang: 'en', isOpenLanguage: false });
        } else {
            this.setState({ selectedLang: 'jp', isOpenLanguage: false });
        }
    }

    handleToggleSwitch = (e) => {
        const { checked } = e.target;
        this.setState({ currentProfileType: checked });
    }

    handleLogin = () => {
        history.push('/login');
    }

    handleSignOut = () => {
        this.props.setUserSignUp({
            name : '', 
            email:'',
            occupation : '',
            ageGroup : '',
            disclose_age : '',
            areaToFollow : [],
            userToFollow : [],
            groupToFollow : [],
            password:'',
            whatNext:''
        });
        let sections = {
            activeSection : "baseRegistration" ,
            currentactive : "baseRegistration"
        }
        this.props.setActiveSection(sections)
        history.push('/');
    }

    // handleLogout = () => {
    //     history.push('/');
    // }

    render() {
        const { en, jp, iconEnClass, iconJpClass, selectedLang } = this.state;
        return (
            <div id="wrapper" className="wrapper home-wrapper">
                <header style={{ display: 'block' }} id="header-set" >
                    <div className="header-div clearfix" >
                        <div className="inner-top-header-div clearfix">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-12 col-md-12">
                                        <div className="header-container">

                                            <div className="logo-div">
                                                <a className="logo_link clearfix" href="/">
                                                    <img src={logo} data-logo-loader className="img-fluid logo_img main-logo" alt="logo" />
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <header className="header-top" id="header-set">
                    <div className="header-div clearfix" >
                        <div className="inner-top-header-div clearfix">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-12 col-md-12">
                                        <div className="header-container">
                                            <div className="logo-div">
                                                <a className="logo_link clearfix" href="/">
                                                    <img src={logoWhite} className="img-fluid logo_img main-logo" alt="logo" />
                                                </a>
                                            </div>
                                            <nav className="nav-center-div ptb-10">
                                                <div className="top-nav1">
                                                    <div className={this.state.isMobileNavBarOpen === true ? "cd-shadow-layer displayblock" : "cd-shadow-layer"}></div>
                                                    <div className="nav-m-bar"><a href="#" onClick={this.handleMobileNavBar} className="opennav" data-placement="bottom" title="" data-original-title="Menu">
                                                        <i className="menu-bars"><img src={menuWhite} alt="menu" /></i></a>
                                                    </div>
                                                    <div className={this.state.isMobileNavBarOpen === true ? "nav-div clearfix width80" : "nav-div clearfix"} id="mySidenav" >
                                                        <a href="#" onClick={this.handleMobileNavBar} className="closebtn">&times;</a>
                                                        <div className="nav-details-right">

                                                            <div className="user-drop-down language-drop-down">
                                                                <div ref={this.setWrapperRefLang} className="dropdown drop-left dropdown-custom-top language-dropdown-top" >
                                                                    <button style={{backgroundColor: 'transparent'}} className="btn btn-default btn-flat dropdown-toggle" onClick={this.handleLanguage} role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        <div className="user-profile">
                                                                            <span className="flag-span"><span className={selectedLang === 'en' ? iconEnClass : iconJpClass}></span> {selectedLang === 'en' ? en : jp} </span>
                                                                        </div>
                                                                    </button>
                                                                    <div className={this.state.isOpenLanguage === true ? "dropdown-menu dropdown-menu-language show" :
                                                                        "dropdown-menu dropdown-menu-language"
                                                                    } aria-labelledby="dropdownMenuLink">
                                                                        <ul>
                                                                            <li><button style={{backgroundColor: 'transparent'}}  className="dropdown-item" onClick={() => this.selectLang('en')} href="#">
                                                                                <span className="flag-span"><span className="flag flag-us"></span> English </span></button>
                                                                            </li>
                                                                            <li><button style={{backgroundColor: 'transparent'}} className="dropdown-item" onClick={() => this.selectLang('jp')} href="#">
                                                                                <span className="flag-span"><span className="flag flag-jp"></span> Japan </span></button>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="user-drop-down">
                                                                <div ref={this.setWrapperRef} className={this.state.isOpenMyAccount === true ? "dropdown drop-left dropdown-custom-top dropdown-without-login show" :
                                                                    "dropdown drop-left dropdown-custom-top dropdown-without-login"} style={{ display: this.state.isUserLogin === false ? 'block' : 'none' }} >
                                                                    <button style={{backgroundColor: 'transparent'}} className="btn btn-default btn-flat dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded={this.state.isOpenMyAccount === true ? "true" : "false"} onClick={this.handleMyAccount}>
                                                                        <div className="user-profile">
                                                                            <div className="user-img">
                                                                                <span className="span-user">
                                                                                    <i className="fe fe-user"></i>
                                                                                </span>
                                                                                {/* <img src="assets/images/user-icon.svg" className="user-top-image" alt="user image" /> */}
                                                                            </div>
                                                                            <div className="user-info">
                                                                                <h3>My account</h3>
                                                                            </div>
                                                                        </div>
                                                                    </button>
                                                                    <div className={this.state.isOpenMyAccount === true ? "dropdown-menu dropdown-menu-without-login show" :
                                                                        "dropdown-menu dropdown-menu-without-login"}
                                                                        style={{ display: this.state.isOpenMyAccount === true ? 'block' : 'none' }} aria-labelledby="dropdownMenuLink">
                                                                        <ul>
                                                                            <li><button style={{backgroundColor: 'transparent'}} className="dropdown-item" href="#" onClick={this.handleLogin}> <span className="icon-span"> <i className="fe fe-log-in icon-i"></i> </span> User login</button></li>
                                                                            <li><button style={{backgroundColor: 'transparent'}} className="dropdown-item" href="/"> <span className="icon-span"> <i className="fe fe-plus icon-i"></i> </span> User registration</button></li>
                                                                        </ul>
                                                                        <ul className="travel-ul">
                                                                            <li><button style={{backgroundColor: 'transparent'}} className="dropdown-item" href="#"> <span className="icon-span"> <i className="material-icons icon-i">store</i> </span> Travel Agency registration</button></li>
                                                                            <li><button style={{backgroundColor: 'transparent'}} className="dropdown-item" href="#"> <span className="icon-span"> <i className="fe fe-log-in icon-i"></i> </span> Host sign in</button></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                                <div ref={this.setWrapperRefProfile} className={this.state.isOpenMyProfile === true ? "dropdown drop-left dropdown-custom-top dropdown-with-login show" : 'dropdown drop-left dropdown-custom-top dropdown-with-login'} style={{ display: this.state.isUserLogin === false ? 'none' : 'block' }}>
                                                                    <button style={{backgroundColor: 'transparent'}} className="btn btn-default btn-flat dropdown-toggle" href="#" onClick={this.handleMyProfile} role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        <div className="user-profile">
                                                                            <div className="user-img">
                                                                                <img src={profile} className="user-top-image" alt="user image" />
                                                                            </div>
                                                                            <div className="user-info">
                                                                                <p>Sign in as</p>
                                                                                <h3>Sunny Minter</h3>
                                                                            </div>
                                                                        </div>
                                                                    </button>
                                                                    <div className={this.state.isOpenMyProfile === true ? "dropdown-menu show" : "dropdown-menu"} aria-labelledby="dropdownMenuLink">

                                                                        <div className="top-switchbar">
                                                                            <div className="custom-control custom-switch">
                                                                                <input type="checkbox" checked={this.state.currentProfileType} onChange={this.handleToggleSwitch} className="custom-control-input" id="switch1" />
                                                                                <label className="custom-control-label" htmlFor="switch1">Switch to <span className={this.state.currentProfileType === false ? "" : "hide-span"} id="user-account">User account</span><span className={this.state.currentProfileType === false ? "hide-span" : ""} id="travel-account">Travel Agency</span></label>
                                                                            </div>
                                                                        </div>

                                                                        <div className={this.state.currentProfileType === false ? "dropdown-agency-panel" : "dropdown-agency-panel hide-panel"} id="agency-panel">
                                                                            <div className="switch-title-heading">
                                                                                <h2>Travel Agency Panel</h2>
                                                                            </div>
                                                                            <ul>
                                                                                <li><button style={{backgroundColor: 'transparent'}} className="dropdown-item" href="#">Dashboard</button></li>
                                                                                <li><button style={{backgroundColor: 'transparent'}} className="dropdown-item" href="#">Invite Host</button></li>
                                                                                <li className="logout-li"><button 
                                                                                onClick={this.handleSignOut} 
                                                                                style={{backgroundColor: 'transparent'}} className="dropdown-item" href="/"> <span className="logout-icon"> <i className="fe fe-log-out"></i> </span> Sign out</button></li>
                                                                            </ul>
                                                                        </div>
                                                                        <div className={this.state.currentProfileType === false ? "dropdown-user-panel hide-panel" : "dropdown-user-panel"} id="user-panel">

                                                                            <div className="switch-title-heading">
                                                                                <h2>User Panel</h2>
                                                                            </div>
                                                                            <ul>
                                                                                <li><button style={{backgroundColor: 'transparent'}} className="dropdown-item" href="#">Timeline</button></li>
                                                                                <li><button style={{backgroundColor: 'transparent'}} className="dropdown-item" href="#">Travel plan Listing</button></li>
                                                                                <li><button style={{backgroundColor: 'transparent'}} className="dropdown-item" href="#">Messages</button></li>
                                                                                <li><button style={{backgroundColor: 'transparent'}} className="dropdown-item" href="#">Profile Edit</button></li>
                                                                                <li><button style={{backgroundColor: 'transparent'}} className="dropdown-item" href="#">Friends Listing</button></li>
                                                                                <li><button style={{backgroundColor: 'transparent'}} className="dropdown-item" href="#">Invite Friends</button></li>
                                                                                <li className="logout-li"><button 
                                                                                onClick={this.handleSignOut} 
                                                                                style={{backgroundColor: 'transparent'}} className="dropdown-item" href="/"> <span className="logout-icon"> <i className="fe fe-log-out"></i> </span> Sign out</button></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setUserSignUp : (data) => dispatch(setUserSignUp(data)),
        setActiveSection : (sections) => dispatch(setActiveSection(sections)),
    }
};

export default connect(null, mapDispatchToProps)(Header);