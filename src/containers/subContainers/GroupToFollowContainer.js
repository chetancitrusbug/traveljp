import {connect} from "react-redux";
import GroupToFollow from "../../component/signUpSection/GroupToFollow";
import { setUserSignUp,setActiveSection,searchGroup } from '../../actionCreators/index';

const mapStoreToProps = (store) => {
    
    return {
        userData : store.userSignUp.userData,
        currentactive : store.activeSection.currentactive,
        allGroupsToFollow : store.userSignUp.groupsToFollow,
        
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUserSignUp : (data) => dispatch(setUserSignUp(data)),
        setActiveSection : (sections) => dispatch(setActiveSection(sections)),
        searchGroup : (search) => dispatch(searchGroup(search)),
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(GroupToFollow);