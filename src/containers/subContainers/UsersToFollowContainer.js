import {connect} from "react-redux";
import UsersToFollow from "../../component/signUpSection/UsersToFollow";
import { setUserSignUp,setActiveSection,searchUser } from '../../actionCreators/index';

const mapStoreToProps = (store) => {
    
    return {
        userData : store.userSignUp.userData,
        currentactive : store.activeSection.currentactive,
        allUsersToFollow : store.userSignUp.usersToFollow
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUserSignUp : (data) => dispatch(setUserSignUp(data)),
        setActiveSection : (sections) => dispatch(setActiveSection(sections)),
        searchUser : (search) => dispatch(searchUser(search)),
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(UsersToFollow);