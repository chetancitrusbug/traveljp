import {connect} from "react-redux";
import AgeGroup from "../../component/signUpSection/AgeGroup";
import { setUserSignUp,setActiveSection } from '../../actionCreators/index';

const mapStoreToProps = (store) => {
    
    return {
        userData : store.userSignUp.userData,
        currentactive : store.activeSection.currentactive
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUserSignUp : (data) => dispatch(setUserSignUp(data)),
        setActiveSection : (sections) => dispatch(setActiveSection(sections)),
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(AgeGroup);