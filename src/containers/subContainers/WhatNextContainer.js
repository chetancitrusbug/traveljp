import {connect} from "react-redux";
import WhatNext from "../../component/signUpSection/WhatNext";
import { setUserSignUp,setActiveSection } from '../../actionCreators/index';

const mapStoreToProps = (store) => {
    
    return {
        userData : store.userSignUp.userData,
        currentactive : store.activeSection.currentactive,
        userId: store.userSignUp.userId,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUserSignUp : (data) => dispatch(setUserSignUp(data)),
        setActiveSection : (sections) => dispatch(setActiveSection(sections)),
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(WhatNext);