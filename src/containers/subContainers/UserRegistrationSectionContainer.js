import {connect} from "react-redux";
import UserRegistrationSection from "../../component/signUpSection/UserRegistrationSection";
import { setUserSignUp,setActiveSection, getUserId } from '../../actionCreators/index';

const mapStoreToProps = (store) => {
    
    return {
        userData : store.userSignUp.userData,
        currentactive : store.activeSection.currentactive
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUserSignUp : (data) => dispatch(setUserSignUp(data)),
        setActiveSection : (sections) => dispatch(setActiveSection(sections)),
        getUserId: (userId) => dispatch(getUserId(userId))
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(UserRegistrationSection);