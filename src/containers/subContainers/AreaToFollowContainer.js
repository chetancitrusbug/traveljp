import {connect} from "react-redux";
import AreaToFollow from "../../component/signUpSection/AreaToFollow";
import { setUserSignUp,setActiveSection,searchArea } from '../../actionCreators/index';

const mapStoreToProps = (store) => {
    
    return {
        userData : store.userSignUp.userData,
        currentactive : store.activeSection.currentactive,
        allAreaToFollow : store.userSignUp.areaToFollow
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUserSignUp : (data) => dispatch(setUserSignUp(data)),
        setActiveSection : (sections) => dispatch(setActiveSection(sections)),
        searchArea : (search) => dispatch(searchArea(search)),
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(AreaToFollow);