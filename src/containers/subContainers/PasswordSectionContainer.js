import {connect} from "react-redux";
import PasswordSection from "../../component/signUpSection/PasswordSection";
import { setUserSignUp,setActiveSection } from '../../actionCreators/index';

const mapStoreToProps = (store) => {
    
    return {
        userData : store.userSignUp.userData,
        currentactive : store.activeSection.currentactive,
        userId: store.userSignUp.userId,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUserSignUp : (data) => dispatch(setUserSignUp(data)),
        setActiveSection : (sections) => dispatch(setActiveSection(sections)),
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(PasswordSection);