import {connect} from "react-redux";
import ChooseOccupation from "../../component/signUpSection/ChooseOccupation";
import { setUserSignUp,setActiveSection } from '../../actionCreators/index';

const mapStoreToProps = (store) => {
    
    return {
        userData : store.userSignUp.userData,
        currentactive : store.activeSection.currentactive
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUserSignUp : (data) => dispatch(setUserSignUp(data)),
        setActiveSection : (sections) => dispatch(setActiveSection(sections)),
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(ChooseOccupation);