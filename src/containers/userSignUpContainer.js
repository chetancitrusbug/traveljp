import {connect} from "react-redux";
import userSignUp from "../pages/SignUpComponent";
// import { OPEN_MODAL_A,OPEN_MODAL_B } from '../../actions/contactActions';
const mapStoreToProps = (store) => {
   return {
    section : store.activeSection,
    currentactive : store.activeSection.currentactive
   }
};

const mapDispatchToProps = (dispatch) => {
    return {
        
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(userSignUp);