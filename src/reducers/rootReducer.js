import {combineReducers} from "redux";
import userSignUp from "./userSignUp";
import activeSection from "./activeSection";

const rootReducer = combineReducers({
    userSignUp,
    activeSection
});

export default rootReducer;