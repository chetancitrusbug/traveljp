import {   GET_USER_SIGN_UP,SET_USER_SIGN_UP,SEARCH_AREA,SEARCH_USER,SEARCH_GROUP, USER__ID } from "../actions/usersSignUpActions";
import {area,user,group} from '../constant/index'

// import _ from "lodash";
const initialState = {
    userData: {
      name : '', 
      email:'',
      occupation : '',
      ageGroup : '',
      disclose_age : false,
      areaToFollow : [],
      userToFollow : [],
      groupToFollow : [],
      password:'',
      whatNext:''
    },
    areaToFollow:area,
    usersToFollow:user,
    groupsToFollow:group
  };
  const userSignUp = (state = {...initialState}, action) => {
    switch(action.type) {
      case GET_USER_SIGN_UP:
        return {
          ...state
        };
      case SET_USER_SIGN_UP:{
        return {
          ...state,
          userData: action.payload
        }; 
      }
      case SEARCH_AREA:{
        let search = action.payload
        let areaSearch = area;
        if(search.length > 0){
          areaSearch = area.slice(0, search.length);
        }
        
        return {
          ...state,
          areaToFollow : areaSearch
        }; 
      }
      case SEARCH_USER:{
        let search = action.payload
        let userSearch = user;
        if(search.length > 0){
          userSearch = user.slice(0, 3);
        }
        
        return {
          ...state,
          usersToFollow:userSearch
        }; 
      }
      case SEARCH_GROUP:{
        let search = action.payload
        let groupSearch = group;
        if(search.length > 0){
          groupSearch = group.slice(0,3);
        }
        
        return {
          ...state,
          groupsToFollow:groupSearch
        }; 
      }
      case USER__ID:{
        return {
          ...state,
          userId: action.payload
        }; 
      }
      default:
        return state;
    }
  };
  
  export default userSignUp;