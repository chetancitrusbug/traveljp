import {   GET_ACTIVE_SECTION, SET_ACTIVE_SECTION } from "../actions/activeSectionActions";
// import _ from "lodash";
const initialState = {
    
    baseRegistration : true,
    chooseOccupation : false,
    ageGroup: false,
    areaToFollow: false,
    usersToFollow: false,
    groupToFollow: false,
    passwordsection: false,
    whatNext: false,
    currentactive : 'baseRegistration'
    
  };
  const userSignUp = (state = {...initialState}, action) => {
    switch(action.type) {
      case GET_ACTIVE_SECTION:
        return {
          ...state
        };
      case SET_ACTIVE_SECTION:{
        return Object.assign({}, state, {
            ...state,
            [`${action.payload.activeSection}`]  : true,
            currentactive: action.payload.currentactive
          })
         
      }
      default:
        return state;
    }
  };
  
  export default userSignUp;