export const GET_USER_SIGN_UP = "GET_USER_SIGN_UP";
export const SET_USER_SIGN_UP = "SET_USER_SIGN_UP";
export const SEARCH_AREA = "SEARCH_AREA";
export const SEARCH_USER = "SEARCH_USER";
export const SEARCH_GROUP = "SEARCH_GROUP";
export const USER__ID = 'USER__ID';