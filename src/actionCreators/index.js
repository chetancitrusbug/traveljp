import * as types from '../actions/usersSignUpActions';
import * as activeSection from '../actions/activeSectionActions';

export const getUserSignUp = () => ({
  type: types.GET_USER_SIGN_UP
});

export const setUserSignUp = (data) => ({
  type: types.SET_USER_SIGN_UP,
  payload : data
});

export const getActiveSection = () => ({
  type: activeSection.GET_ACTIVE_SECTION
});

export const setActiveSection = (payload) => ({
  type: activeSection.SET_ACTIVE_SECTION,
  payload:payload
});

export const searchArea = (search) => ({
  type:types.SEARCH_AREA,
  payload:search
})
export const searchUser = (search) => ({
  type:types.SEARCH_USER,
  payload:search
})

export const searchGroup = (search) => ({
  type:types.SEARCH_GROUP,
  payload:search
})

export const getUserId = (userId) => ({
  type: types.USER__ID,
  payload: userId,
}) 