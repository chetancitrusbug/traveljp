import React ,  { Fragment }  from 'react';
import { Router, Route, Redirect, Switch } from 'react-router-dom';
import history from './services/BrowserHistory';
import SignUpComponent from './containers/userSignUpContainer'
import LoginComponent from './containers/userLoginContainer'
import PlannersComponent from './containers/PlannersContainer'
import DashboardComponent from './containers/DashboardContainer'
import MappersNewComponent from './containers/MappersNewContainer'

import { Provider } from 'react-redux';
import { store } from './services/Redux';
class App extends React.Component {
  componentDidMount(){
    if(localStorage.getItem("language") === null){
        localStorage.setItem("language","eng")
    }
  }
  render() {
    return (
      <Provider store={store}>
        <Fragment>
          <Router history={history}>
            <Switch>
              <Route exact path="/" component={SignUpComponent} />
              <Route exact path="/login" component={LoginComponent} />
              <Route exact path="/planners/3_spots_planner" component={PlannersComponent} />
              <Route exact path="/dashboard/:userId" component={DashboardComponent} />
              <Route exact path="/mappers/new" component={MappersNewComponent} />
              
              <Route render={() => <Redirect to="/" />} />
            </Switch>
          </Router> 
        </Fragment>
      </Provider>
      
    );
  }
}

export default App;