import React from 'react';
import {lanuguage} from '../../config/config'
import lang from '../../constant/lang';
import { getOccupation } from '../../services/ApiServices';
import $ from "jquery";
class ChooseOccupation extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            occupationJson: [],
            name : this.props.userData.name,
            email : this.props.userData.email,
            occupation : (this.props.userData.occupation !== '' && this.props.userData.occupation !== undefined) ? this.props.userData.occupation : '',
            ageGroup : (this.props.userData.ageGroup !== '' && this.props.userData.ageGroup !== undefined) ? this.props.userData.ageGroup : 0,
            disclose_age: (this.props.userData.disclose_age !== '' && this.props.userData.disclose_age !== undefined) ? this.props.userData.disclose_age : false,
            areaToFollow:(this.props.userData.areaToFollow !== '' && this.props.userData.areaToFollow !== undefined) ? this.props.userData.areaToFollow : [],
            userToFollow:(this.props.userData.userToFollow !== '' && this.props.userData.userToFollow !== undefined) ? this.props.userData.userToFollow : [],
            groupToFollow:(this.props.userData.groupToFollow !== '' && this.props.userData.groupToFollow !== undefined) ? this.props.userData.groupToFollow : [],
            password : (this.props.userData.password !== '' && this.props.userData.password !== undefined) ? this.props.userData.password : '',
            whatNext: (this.props.userData.whatNext !== '' && this.props.userData.whatNext !== undefined) ? this.props.userData.whatNext : '',
        }

    }

    async componentDidMount() {
        try {
            const res = await getOccupation();
            const code = res.code;
            switch (code) {
                case 200:
                    this.setState({occupationJson: res.data});
                    break;
                case 400:
    
                    break;
            
                default:
                    break;
            }
        } catch (error) {
            
        }
    }
    
    handleOnChange = (e) => {
        this.setState({
            occupation : e.target.value
        }, () => {
            this.props.setUserSignUp({
                name : this.state.name, 
                email:this.state.email,
                occupation : this.state.occupation,
                ageGroup : this.state.ageGroup,
                disclose_age : this.state.disclose_age,
                areaToFollow : this.state.areaToFollow,
                userToFollow : this.state.userToFollow,
                groupToFollow : this.state.groupToFollow,
                password:this.state.password,
                whatNext:this.state.whatNext
            });
            let sections = {
                activeSection : "ageGroup" ,
                currentactive : "ageGroup"
            }
            this.props.setActiveSection(sections)
     
        })
    }

    handleClickNext = () => {
        this.props.setUserSignUp({
            name : this.state.name, 
            email:this.state.email,
            occupation : this.state.occupation,
            ageGroup : this.state.ageGroup,
            disclose_age : this.state.disclose_age,
            areaToFollow : this.state.areaToFollow,
            userToFollow : this.state.userToFollow,
            groupToFollow : this.state.groupToFollow,
            password:this.state.password,
            whatNext:this.state.whatNext
        });
        let sections = {
            activeSection : "ageGroup" ,
            currentactive : "ageGroup"
        }
        this.props.setActiveSection(sections)

    }

    handleClickPreviouse = () => {
        this.props.setUserSignUp({
            name : this.state.name, 
            email:this.state.email,
            occupation : this.state.occupation,
            ageGroup : this.state.ageGroup,
            disclose_age : this.state.disclose_age,
            areaToFollow : this.state.areaToFollow,
            userToFollow : this.state.userToFollow,
            groupToFollow : this.state.groupToFollow,
            password:this.state.password,
            whatNext:this.state.whatNext
        });
        let sections = {
            activeSection : "baseRegistration" ,
            currentactive : "baseRegistration"
        }
        this.props.setActiveSection(sections);

    }
    handleSkip = () =>{
        let sections = {
            activeSection : "whatNext" ,
            currentactive : "whatNext"
        }
        this.props.setActiveSection(sections)
    }
    
    render (){
        let occupation = lang.occupation;
        const { occupationJson } = this.state;
        return (
            <div className={"common-div occupation-div chooseOccupation"+ (this.props.currentactive !== 'chooseOccupation' ? ' setOpacity' : '')}>
                <div className="container">
                    <div className="row">
    
                        <div className="col-lg-12 col-md-12">        
    
                            <div className="common-card-div">
                                
                                <div className="heading-top-row">
                                    <div className="heading-div"> 
                                        <h2>{occupation.header}</h2>
                                        <p>{occupation.subHeading}</p>
                                    </div>
                                </div>

                                <div className="main-body-div">

                                    <div className="select-img-root">
                                        <div className="row get-row mr-minus-8">
                                            {occupationJson.length > 0 &&  occupationJson.map((value,key) => <div key={key}className="col-lg-3 col-md-3 grid-20 plr-8">
                                                <div className={"radio-card-box occupation-card-box"+ (this.state.occupation === value.slug ? ' active' : ' disabled')}>
                                                    <label htmlFor={"opt-radio"+value.id} className="label-box">
                                                        <div className="icon-img-thumb">
                                                            <i className={"custom-icon "+value.fontawesome}></i>
                                                        </div>
                                                        <div className="title-row">
                                                            <p>{(lanuguage === 'eng') ? value.label : value.localLabel}</p>
                                                        </div>
                                                    </label>
                                                    <input type="radio" value={value.slug} onChange={this.handleOnChange} className="form-radio form-radio-occupation" id={"opt-radio"+value.id} name="occupation-select" checked={this.state.occupation === value.slug} />
                                                </div>
                                            </div>)}
                                       </div>
                                    </div>

                                </div>
                                {this.props.currentactive === 'chooseOccupation' &&
                                    <div className="footer-bottom-row">
                                        <div className="container">
                                            <div className="row mr-minus-8">
                                                <div className="col-lg-12 plr-8">
                                                    <div className="button-row">
                                                        <div className="btn-left-side justify-content-between"> 
                                                            <button onClick={this.handleClickPreviouse}  className="btn btn-outline-primary btn-common btn-previous">
                                                                <i className="fe fe-arrow-left fe-icon"></i>
                                                                <span className="text-span">{lang.previous}</span>
                                                            </button>
                                                            <a href="#" onClick={this.handleSkip} className="btn btn-link btn-skip mr-30">{lang.skip[0]} <span className="remove-mobile">{lang.skip[1]}</span>{lang.skip[2]}!</a>
                                                        </div>
                                                        <div className="btn-right-side"> 
                                                            <button onClick={this.handleClickNext} className="btn btn-primary btn-common btn-continue">
                                                                <span className="text-span">{lang.continue}</span>
                                                                <i className="fe fe-arrow-right fe-icon"></i>
                                                            </button>
                                                        </div>    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }

                            </div>
                            
                        </div>  
                        
                    </div>        
                </div>
            </div> 
            
        );
    }
}

export default ChooseOccupation;