import React from 'react';
import googlePlus from '../../assets/images/icons/Google+.svg'
import facebook from '../../assets/images/icons/Facebook.svg'
import lang from '../../constant/lang';
import { googleSignupService, facebookSignupService, simpleSignupService } from '../../services/ApiServices';

class UserRegistrationSection extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            name : (this.props.userData.name !== '' && this.props.userData.name !== undefined) ? this.props.userData.name : '' ,
            email : (this.props.userData.email !== '' && this.props.userData.email !== undefined) ? this.props.userData.email : '',
            emailError : false,
            occupation : (this.props.userData.occupation !== '' && this.props.userData.occupation !== undefined) ? this.props.userData.occupation : '',
            ageGroup : (this.props.userData.ageGroup !== '' && this.props.userData.ageGroup !== undefined) ? this.props.userData.ageGroup : 0,
            disclose_age: (this.props.userData.disclose_age !== '' && this.props.userData.disclose_age !== undefined) ? this.props.userData.disclose_age : false,
            areaToFollow:(this.props.userData.areaToFollow !== '' && this.props.userData.areaToFollow !== undefined) ? this.props.userData.areaToFollow : [],
            userToFollow:(this.props.userData.userToFollow !== '' && this.props.userData.userToFollow !== undefined) ? this.props.userData.userToFollow : [],
            groupToFollow:(this.props.userData.groupToFollow !== '' && this.props.userData.groupToFollow !== undefined) ? this.props.userData.groupToFollow : [],
            password : (this.props.userData.password !== '' && this.props.userData.password !== undefined) ? this.props.userData.password : '',
            whatNext: (this.props.userData.whatNext !== '' && this.props.userData.whatNext !== undefined) ? this.props.userData.whatNext : '',
        }

    }
    
    handleOnChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    handleClickNext = async () => {
        if(this.state.email === ''){
            this.setState({
                emailError : true
            })
        }else{
            const userData = {
                name : this.state.name, 
                email:this.state.email,
            }
            const res = await simpleSignupService(userData);
            if (res) {
                if (res.code === 200 && res.status === true) {
                    this.props.getUserId(res.data.user_id);
                    this.setState({
                        emailError : false
                    })
                    this.props.setUserSignUp({
                        name : this.state.name, 
                        email:this.state.email,
                        occupation : this.state.occupation,
                        ageGroup : this.state.ageGroup,
                        disclose_age : this.state.disclose_age,
                        areaToFollow : this.state.areaToFollow,
                        userToFollow : this.state.userToFollow,
                        groupToFollow : this.state.groupToFollow,
                        password:this.state.password,
                        whatNext:this.state.whatNext
                    });
                    let sections = {
                        activeSection : "chooseOccupation" ,
                        currentactive : "chooseOccupation"
                    }
                    this.props.setActiveSection(sections)
                }
            }
        }
    }

    handleGoogleLogin = async () => {
        const res = await googleSignupService();
        if (res) {
            if (res.code === 200 && res.status === true) {
                this.props.getUserId(res.data.user_id);
                this.props.setUserSignUp({
                    name : this.state.name, 
                    email:this.state.email,
                    occupation : this.state.occupation,
                    ageGroup : this.state.ageGroup,
                    disclose_age : this.state.disclose_age,
                    areaToFollow : this.state.areaToFollow,
                    userToFollow : this.state.userToFollow,
                    groupToFollow : this.state.groupToFollow,
                    password:this.state.password,
                    whatNext:this.state.whatNext
                });
                let sections = {
                    activeSection : "chooseOccupation" ,
                    currentactive : "chooseOccupation"
                }
                this.props.setActiveSection(sections)
            }
        }
    }

    handleFacebookLogin = async () => {
        const res = await facebookSignupService();
        if (res) {
            if (res.code === 200 && res.status === true) {
                this.props.getUserId(res.data.user_id);
                this.props.setUserSignUp({
                    name : this.state.name, 
                    email:this.state.email,
                    occupation : this.state.occupation,
                    ageGroup : this.state.ageGroup,
                    disclose_age : this.state.disclose_age,
                    areaToFollow : this.state.areaToFollow,
                    userToFollow : this.state.userToFollow,
                    groupToFollow : this.state.groupToFollow,
                    password:this.state.password,
                    whatNext:this.state.whatNext
                });
                let sections = {
                    activeSection : "chooseOccupation" ,
                    currentactive : "chooseOccupation"
                }
                this.props.setActiveSection(sections)
            }
        }
    }



    render (){
        let registration = lang.registration;
        return (
            <div className={"common-div user-registration-div baseRegistration"+ (this.props.currentactive !== 'baseRegistration' ? ' setOpacity' : '')}>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-md-12">        
                            <div className="common-card-div">
                                <div className="heading-top-row">
                                    <div className="heading-div"> 
                                        <h2>{registration.title}</h2>
                                        <p>{registration.subHeading}</p>
                                    </div>
                                </div>

                                <div className="main-body-div main-body-div2 pr-80">

                                    <div className="login-div">
                                        <div className="row">
                                            <div className="col-lg-6 col-md-6">
                                                <div className="form-div">
                                                    <div className="form-group">
                                                        <label htmlFor="">{registration.name}</label>
                                                        <input type="text" className="form-control" name="name" placeholder={registration.namePlaceHolder} onChange={this.handleOnChange} />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="">{registration.Email}</label>
                                                        <input type="email" name="email" className="form-control" placeholder={registration.emailPlaceHolder} onChange={this.handleOnChange} />
                                                        <span className={"help-error font-italic " + (this.state.emailError ? " " : "display-none")} >
                                                            <span className="text-underline font-500">{registration.emailError[0]}</span>{registration.emailError[1]}
                                                        </span>
                                                    </div>
                                                </div>

                                                <div className="checkbox-select-root checkbox-select-2">
                                                    <div className="custom-control custom-checkbox">
                                                        <input type="checkbox" className="custom-control-input" id="customCheck" name="example1" />
                                                        <label className="custom-control-label" htmlFor="customCheck">
                                                        <a href="/terms">Terms</a> &   Conditions and <a href="/privacy">privacy</a> policy
                                                        </label>
                                                    </div>
                                                </div>
                                            

                                                <div className="social-row">
                                                    <div className="left-social-txt">
                                                        <p> <span className="text-span">{registration.loginText[0]} </span> {registration.loginText[1]}</p>
                                                    </div>
                                                    <div className="social-button-row">
                                                        <a href="#" onClick={this.handleGoogleLogin} className="btn btn-social btn-google-plus mr-10">
                                                            <img src={googlePlus} className="img-fluid img-social img-google" alt="Google+" />
                                                        </a>
                                                        <a href="#" onClick={this.handleFacebookLogin} className="btn btn-social btn-facebook">
                                                            <img src={facebook} className="img-fluid img-social img-facebook" alt="Facebook" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-md-6">
                                                <div className="description-div">
                                                    <h4>{registration.question}<span className="text-bold">{registration.siteTitle}</span></h4>
                                                    <div className="list-info-div">
                                                        <ul>
                                                            {registration.answers.map((value,key) => <li key={key}>{registration.answers[key]}</li>
                                                            )}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {this.props.currentactive === 'baseRegistration' &&
                                    <div className="footer-bottom-row footer-bottom-row-old">
                                        
                                        <div className="button-row justify-content-end">
                                            <div className="btn-right-side"> 
                                                <button onClick={this.handleClickNext} className="btn btn-primary btn-common btn-continue">
                                                <span className="text-span">{lang.continue}</span>
                                                    <i className="fe fe-arrow-right fe-icon"></i>
                                                </button>
                                            </div>    
                                        </div>
                            
                                    </div>
                                }

                            </div>
    
                        </div>  
                        
                    </div>        
                </div>
            </div> 
        );
    }
}

export default UserRegistrationSection;