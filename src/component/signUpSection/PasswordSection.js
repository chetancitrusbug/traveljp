import React from 'react';
import lang from '../../constant/lang';
import { updatePasswordService } from '../../services/ApiServices';
class PasswordSection extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            name : this.props.userData.name,
            email : this.props.userData.email,
            occupation : (this.props.userData.occupation !== '' && this.props.userData.occupation !== undefined) ? this.props.userData.occupation : '',
            ageGroup : (this.props.userData.ageGroup !== '' && this.props.userData.ageGroup !== undefined) ? this.props.userData.ageGroup : 0,
            disclose_age: (this.props.userData.disclose_age !== '' && this.props.userData.disclose_age !== undefined) ? this.props.userData.disclose_age : false,
            areaToFollow:(this.props.userData.areaToFollow !== '' && this.props.userData.areaToFollow !== undefined) ? this.props.userData.areaToFollow : [],
            userToFollow:(this.props.userData.userToFollow !== '' && this.props.userData.userToFollow !== undefined) ? this.props.userData.userToFollow : [],
            groupToFollow:(this.props.userData.groupToFollow !== '' && this.props.userData.groupToFollow !== undefined) ? this.props.userData.groupToFollow : [],
            password : (this.props.userData.password !== '' && this.props.userData.password !== undefined) ? this.props.userData.password : '',
            confirmPassword : (this.props.userData.password !== '' && this.props.userData.password !== undefined) ? this.props.userData.password : '',
            passwordType:true,
            confirmPasswordType:true,
            passwordError : false,
            whatNext: (this.props.userData.whatNext !== '' && this.props.userData.whatNext !== undefined) ? this.props.userData.whatNext : '',
            userId: undefined,
        }

    }

    componentDidMount() {
        this.setState({ userId: this.props.userId});
    }
    
    handleOnChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    handlePasswordShow = (pwd) => {
        if(pwd === 'confirmPassword'){
            this.setState({
                confirmPasswordType : !this.state.confirmPasswordType
            })
        }
        if(pwd === 'password'){
            this.setState({
                passwordType : !this.state.passwordType
            })
        }
    }

    handleClickNext = async () => {
        if(this.state.password === this.state.confirmPassword && this.state.password !== '' && this.state.confirmPassword !== ''){
            const userData = {
                password: this.state.password,
            };
            const res = await updatePasswordService(this.state.userId, userData);
            if (res) {
                if (res.code === 200 && res.status === true) {
                    this.props.setUserSignUp({
                        name : this.state.name, 
                        email:this.state.email,
                        occupation : this.state.occupation,
                        ageGroup : this.state.ageGroup,
                        disclose_age : this.state.disclose_age,
                        areaToFollow : this.state.areaToFollow,
                        userToFollow : this.state.userToFollow,
                        groupToFollow : this.state.groupToFollow,
                        password:this.state.password,
                        whatNext:this.state.whatNext
                    });
                    let sections = {
                        activeSection : "whatNext" ,
                        currentactive : "whatNext"
                    }
                    this.props.setActiveSection(sections)
                    this.setState({
                        passwordError : false
                    })
                }
            }
        }else{
            this.setState({
                passwordError : true
            })
        }
    }
    
    handleSkip = () =>{
        let sections = {
            activeSection : "whatNext" ,
            currentactive : "whatNext"
        }
        this.props.setActiveSection(sections)
    }

    handleClickPreviouse = () => {
        this.props.setUserSignUp({
            name : this.state.name, 
            email:this.state.email,
            occupation : this.state.occupation,
            ageGroup : this.state.ageGroup,
            disclose_age : this.state.disclose_age,
            areaToFollow : this.state.areaToFollow,
            userToFollow : this.state.userToFollow,
            groupToFollow : this.state.groupToFollow,
            password:this.state.password,
            whatNext:this.state.whatNext
        });
        let sections = {
            activeSection : "groupToFollow" ,
            currentactive : "groupToFollow"
        }
        this.props.setActiveSection(sections)
    }
    render (){
        let passwordSectionLang = lang.passwordSection;
        return (
            <div className={"common-div password-div passwordsection"+ (this.props.currentactive !== 'passwordsection' ? ' setOpacity' : '')}>
                <div className="container">
                    <div className="row">
    
                        <div className="col-lg-12 col-md-12">        
    
                            <div className="common-card-div">
                                
                            <div className="heading-top-row">
                                    <div className="heading-div"> 
                                        <h2>{passwordSectionLang.header}</h2>
                                        <p>{passwordSectionLang.subHeading}</p>
                                    </div>
                                </div>

                                <div className="main-body-div main-body-div2">

                                    <div className="login-div">
                                        <div className="row mr-minus-8">
                                            <div className="col-lg-6 col-md-6 plr-8">
                                                <div className="form-div">
                                                    <div className="form-group">
                                                        <label htmlFor="">{passwordSectionLang.password}</label>
                                                        <div className="input-group-box password-box">
                                                            <input type={this.state.passwordType ? "password" : "text"} className="form-control" name="password" onChange={this.handleOnChange} id="password" placeholder={passwordSectionLang.password} value={this.state.password}/>
                                                            <button onClick={() =>this.handlePasswordShow('password')} type="button" id="show_password" name="show_password" className="pass-hide password-view-click"><i className="material-icons password-view">remove_red_eye</i></button> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-md-6 plr-8">
                                                <div className="form-div">
                                                    <div className="form-group">
                                                        <label htmlFor="">{passwordSectionLang.confirmPassword}</label>
                                                        <div className="input-group-box password-box">
                                                            <input type={this.state.confirmPasswordType ? "password" : "text"}  className="form-control" name="confirmPassword"  id="confirm-password" placeholder={passwordSectionLang.confirmPasswordPlaceHolder} value={this.state.confirmPassword} onChange={this.handleOnChange}/>
                                                            <button type="button" id="show_password2" onClick={() =>this.handlePasswordShow('confirmPassword')} name="show_password" className="pass-hide password-view-click"><i className="material-icons password-view">remove_red_eye</i></button> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <span className={"help-error font-italic " + (this.state.passwordError ? " " : "display-none")} >
                                                <span className="text-underline font-500">{passwordSectionLang.passwordError[0]}</span> {passwordSectionLang.passwordError[1]}
                                            </span>
                                        </div>
                                    </div>

                                </div>

                                {this.props.currentactive === 'passwordsection' &&
                                    <div className="footer-bottom-row">
                                        <div className="container">
                                            <div className="row mr-minus-8">
                                                <div className="col-lg-12 plr-8">
                                                    <div className="button-row">
                                                        <div className="btn-left-side justify-content-between"> 
                                                            <button onClick={this.handleClickPreviouse}  className="btn btn-outline-primary btn-common btn-previous">
                                                                <i className="fe fe-arrow-left fe-icon"></i>
                                                                <span className="text-span">{lang.previous}</span>
                                                            </button>
                                                            <a href="#" onClick={this.handleSkip} className="btn btn-link btn-skip mr-30">{lang.skip[0]} <span className="remove-mobile">{lang.skip[1]}</span>{lang.skip[2]}!</a>
                                                        </div>
                                                        <div className="btn-right-side"> 
                                                            <button onClick={this.handleClickNext} className="btn btn-primary btn-common btn-continue">
                                                                <span className="text-span">{lang.continue}</span>
                                                                <i className="fe fe-arrow-right fe-icon"></i>
                                                            </button>
                                                        </div>    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>  
                    </div>        
                </div>
            </div> 
        );
    }
}

export default PasswordSection;