import React from 'react';
import lang from '../../constant/lang';
import {lanuguage} from '../../config/config'
import { getWhatToNext, SignUpService } from '../../services/ApiServices';
import history from '../../services/BrowserHistory';
// const whatNextData = [
//     {
//         id:1,
//         name:"Proceed to Timeline",
//         slug:"to-timeline",
//         localName:"タイムラインに進む",
//         icon:"proceed-timeline-icon"
//     },
//     {
//         id:2,
//         name:"Create a Map",
//         slug:"create-map",
//         localName:"地図を作成する",
//         icon:"create-map-icon"
//     },
//     {
//         id:3,
//         name:"Create a travel plan",
//         slug:"create-travel-plan",
//         localName:"旅行プランを作成する",
//         icon:"travel-plan-icon"
//     }
// ]
class WhatNext extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            whatNextData: [],
            name : this.props.userData.name,
            email : this.props.userData.email,
            occupation : (this.props.userData.occupation !== '' && this.props.userData.occupation !== undefined) ? this.props.userData.occupation : '',
            ageGroup : (this.props.userData.ageGroup !== '' && this.props.userData.ageGroup !== undefined) ? this.props.userData.ageGroup : 0,
            disclose_age: (this.props.userData.disclose_age !== '' && this.props.userData.disclose_age !== undefined) ? this.props.userData.disclose_age : false,
            areaToFollow:(this.props.userData.areaToFollow !== '' && this.props.userData.areaToFollow !== undefined) ? this.props.userData.areaToFollow : [],
            userToFollow:(this.props.userData.userToFollow !== '' && this.props.userData.userToFollow !== undefined) ? this.props.userData.userToFollow : [],
            groupToFollow:(this.props.userData.groupToFollow !== '' && this.props.userData.groupToFollow !== undefined) ? this.props.userData.groupToFollow : [],
            password : (this.props.userData.password !== '' && this.props.userData.password !== undefined) ? this.props.userData.password : '',
            whatNext: (this.props.userData.whatNext !== '' && this.props.userData.whatNext !== undefined) ? this.props.userData.whatNext : '',
            userId: undefined,
        }

    }

  async componentDidMount() {
         try {
            const res = await getWhatToNext();
            const code = res.code;
            switch (code) {
                case 200:
                    this.setState({ whatNextData: res.data });
                    break;
                case 400:

                    break;

                default:
                    break;
            }
        } catch (error) {

        }
        this.setState({ userId: this.props.userId});
    }
    
    handleOnChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        }, async () => {
            const {whatNext} = this.state;
            try {
                const res = await SignUpService(this.state);
                const code = res.code;
                switch (code) {
                    case 200:
                    switch (whatNext) {
                        case "create-map":
                            history.push('/mappers/new');
                            break;
                        case "to-timeline":
                            history.push(`/dashboard/${this.state.userId}`);
                            break;
                        case "create-travel-plan":
                            history.push('/planners/3_spots_planner');
                        break;
                        default:
                            break;
                    }
                        break;
                    case 400:
    
                        break;
    
                    default:
                        break;
                }
            } catch (error) {
    
            }
        })
    }

    handleClickNext = async () => {
        this.props.setUserSignUp({
            name : this.state.name, 
            email:this.state.email,
            occupation : this.state.occupation,
            ageGroup : this.state.ageGroup,
            disclose_age : this.state.disclose_age,
            areaToFollow : this.state.areaToFollow,
            userToFollow : this.state.userToFollow,
            groupToFollow : this.state.groupToFollow,
            password:this.state.password,
            whatNext:this.state.whatNext
        });
        let sections = {
            activeSection : "whatNext" ,
            currentactive : "whatNext"
        }
        this.props.setActiveSection(sections)

        try {
            const res = await SignUpService(this.state);
            const code = res.code;
            switch (code) {
                case 200:
                   alert('Form Submitted...!!');
                    break;
                case 400:

                    break;

                default:
                    break;
            }
        } catch (error) {

        }
    }

    handleClickPreviouse = () => {
        this.props.setUserSignUp({
            name : this.state.name, 
            email:this.state.email,
            occupation : this.state.occupation,
            ageGroup : this.state.ageGroup,
            disclose_age : this.state.disclose_age,
            areaToFollow : this.state.areaToFollow,
            userToFollow : this.state.userToFollow,
            groupToFollow : this.state.groupToFollow,
            password:this.state.password,
            whatNext:this.state.whatNext
        });
        let sections = {
            activeSection : "passwordsection" ,
            currentactive : "passwordsection"
        }
        this.props.setActiveSection(sections)
    }
    render (){
        let whatNextLang = lang.whatNextLang;
        const { whatNextData } = this.state;
        return (
            <div className={"common-div occupation-div whatNext"+ (this.props.currentactive !== 'whatNext' ? ' setOpacity' : '')}>
                <div className="container">
                    <div className="row">
    
                        <div className="col-lg-12 col-md-12">        
    
                            <div className="common-card-div">
                                
                                <div className="heading-top-row">
                                    <div className="heading-div"> 
                                        <h2>{whatNextLang.header}</h2>
                                        <p>{whatNextLang.subHeading}</p>
                                    </div>
                                </div>

                                <div className="main-body-div">

                                    <div className="select-img-root">
                                        <div className="row get-row mr-minus-8 justify-content-center">
                                            {
                                                whatNextData.map((value,key)=>
                                                <div key={key} className="col-lg-3 col-md-3 grid-20 plr-8">
                                                
                                                    <div className={"radio-card-box what-to-do-card-box" + (this.state.whatNext === value.slug ? ' active' : '')}>
                                                        <label htmlFor={"what-to-do-radio"+value.id} className="label-box">
                                                            <div className="icon-img-thumb">
                                                                <i className={"custom-icon "+ value.icon}></i>
                                                            </div>
                                                            <div className="title-row">
                                                                <p>{(lanuguage === 'eng') ? value.name : value.localName}</p>
                                                            </div>
                                                        </label>
                                                        <input type="radio" className="form-radio form-radio-what-to-do" id={"what-to-do-radio"+value.id} name="whatNext"  onChange={this.handleOnChange} value={value.slug} />
                                                    </div>
                                                </div>
                                                )
                                            }
                                            
                                        </div>
                                    </div>

                                </div>

                                {/* {this.props.currentactive === 'whatNext' &&
                                    <div className="footer-bottom-row">
                                    <div className="container">
                                        <div className="row mr-minus-8">
                                            <div className="col-lg-12 plr-8">
                                                <div className="button-row">
                                                    <div className="btn-left-side justify-content-between"> 
                                                        <button onClick={this.handleClickPreviouse}  className="btn btn-outline-primary btn-common btn-previous">
                                                            <i className="fe fe-arrow-left fe-icon"></i>
                                                            <span className="text-span">{lang.previous}</span>
                                                        </button>
                                                    </div>
                                                    <div className="btn-right-side"> 
                                                        <button onClick={this.handleClickNext} className="btn btn-primary btn-common btn-continue">
                                                            <span className="text-span">{lang.continue}</span>
                                                            <i className="fe fe-arrow-right fe-icon"></i>
                                                        </button>
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                } */}
                            </div>
                        </div>  
                    </div>        
                </div>
            </div> 
        );
    }
}

export default WhatNext;