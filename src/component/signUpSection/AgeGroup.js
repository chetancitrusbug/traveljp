import React from 'react';
// import {lanuguage} from '../../config/config'
import lang from '../../constant/lang';
import { getAgeGroup } from '../../services/ApiServices';

class AgeGroup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ageData: [],
            name: this.props.userData.name,
            email: this.props.userData.email,
            occupation: (this.props.userData.occupation !== '' && this.props.userData.occupation !== undefined) ? this.props.userData.occupation : '',
            ageGroup: (this.props.userData.ageGroup !== '' && this.props.userData.ageGroup !== undefined) ? this.props.userData.ageGroup : 0,
            disclose_age: (this.props.userData.disclose_age !== '' && this.props.userData.disclose_age !== undefined) ? this.props.userData.disclose_age : false,
            areaToFollow: (this.props.userData.areaToFollow !== '' && this.props.userData.areaToFollow !== undefined) ? this.props.userData.areaToFollow : [],
            userToFollow: (this.props.userData.userToFollow !== '' && this.props.userData.userToFollow !== undefined) ? this.props.userData.userToFollow : [],
            groupToFollow: (this.props.userData.groupToFollow !== '' && this.props.userData.groupToFollow !== undefined) ? this.props.userData.groupToFollow : [],
            password: (this.props.userData.password !== '' && this.props.userData.password !== undefined) ? this.props.userData.password : '',
            whatNext: (this.props.userData.whatNext !== '' && this.props.userData.whatNext !== undefined) ? this.props.userData.whatNext : '',
        }
    }

    async componentDidMount() {
        try {
            const res = await getAgeGroup();
            const code = res.code;
            switch (code) {
                case 200:
                    this.setState({ ageData: res.data });
                    break;
                case 400:

                    break;

                default:
                    break;
            }
        } catch (error) {

        }
    }

    handleOnChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        }, () => {
            this.props.setUserSignUp({
                name: this.state.name,
                email: this.state.email,
                occupation: this.state.occupation,
                ageGroup: this.state.ageGroup,
                disclose_age: this.state.disclose_age,
                areaToFollow: this.state.areaToFollow,
                userToFollow: this.state.userToFollow,
                groupToFollow: this.state.groupToFollow,
                password: this.state.password,
                whatNext: this.state.whatNext
            });
            let sections = {
                activeSection: "areaToFollow",
                currentactive: "areaToFollow"
            }
            this.props.setActiveSection(sections)
        })
    }

    handleClickNext = () => {
        this.props.setUserSignUp({
            name: this.state.name,
            email: this.state.email,
            occupation: this.state.occupation,
            ageGroup: this.state.ageGroup,
            disclose_age: this.state.disclose_age,
            areaToFollow: this.state.areaToFollow,
            userToFollow: this.state.userToFollow,
            groupToFollow: this.state.groupToFollow,
            password: this.state.password,
            whatNext: this.state.whatNext
        });
        let sections = {
            activeSection: "areaToFollow",
            currentactive: "areaToFollow"
        }
        this.props.setActiveSection(sections)
    }

    handleClickPreviouse = () => {
        this.props.setUserSignUp({
            name: this.state.name,
            email: this.state.email,
            occupation: this.state.occupation,
            ageGroup: this.state.ageGroup,
            disclose_age: this.state.disclose_age,
            areaToFollow: this.state.areaToFollow,
            userToFollow: this.state.userToFollow,
            groupToFollow: this.state.groupToFollow,
            password: this.state.password,
            whatNext: this.state.whatNext
        });
        let sections = {
            activeSection: "chooseOccupation",
            currentactive: "chooseOccupation"
        }
        this.props.setActiveSection(sections)
    }

    handleSkip = () => {
        let sections = {
            activeSection: "whatNext",
            currentactive: "whatNext"
        }
        this.props.setActiveSection(sections)
    }

    render() {
        let ageLang = lang.ageGroup;
        const { ageData } = this.state;
        return (
            <div className={"common-div occupation-div ageGroup" + (this.props.currentactive !== 'ageGroup' ? ' setOpacity' : '')}>
                <div className="container">
                    <div className="row">

                        <div className="col-lg-12 col-md-12">

                            <div className="common-card-div">

                                <div className="heading-top-row">
                                    <div className="heading-div">
                                        <h2>{ageLang.header}</h2>
                                        <p>{ageLang.subHeading}</p>
                                    </div>
                                </div>

                                <div className="main-body-div main-body-div2">

                                    <div className="select-img-root select-label-root">
                                        <div className={"row get-row mr-minus-8 "}>
                                            {ageData.length > 0 && ageData.map((data, index) => {
                                                return <div key={index} className={"col-lg-2 col-md-2 grid-125 plr-8 "}>
                                                    <div className={"radio-card-box age-group-card-box "}>
                                                        <label htmlFor={data.id}  className="label-box">
                                                            <div className="title-row">
                                                                {data.age === 19 ?
                                                                    <p>-{data.age}</p>
                                                                    : data.age === 60 ?
                                                                        <p>{data.age}+</p>
                                                                        : <p>{data.age}</p>
                                                                }
                                                            </div>
                                                        </label>
                                                        <input type="radio" className="form-radio form-radio-age-group" id={data.id} name="ageGroup" value={data.age} onClick={this.handleOnChange} />
                                                    </div>
                                                </div>
                                            })}
                                          </div>
                        
                                        <div className="row get-row mr-minus-8">
                                            <div className="col-lg-12 col-md-12 plr-8">
                                                <div className="checkbox-select-root">
                                                    <div className="custom-control custom-checkbox">
                                                        <input type="checkbox" onClick={this.handleOnChange} className="custom-control-input" id="customCheck" name="disclose_age" />
                                                        <label className="custom-control-label" htmlFor="customCheck">{ageLang.hideAge}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                {this.props.currentactive === 'ageGroup' &&
                                    <div className="footer-bottom-row">
                                        <div className="container">
                                            <div className="row mr-minus-8">
                                                <div className="col-lg-12 plr-8">
                                                    <div className="button-row">
                                                        <div className="btn-left-side justify-content-between">
                                                            <button onClick={this.handleClickPreviouse} className="btn btn-outline-primary btn-common btn-previous">
                                                                <i className="fe fe-arrow-left fe-icon"></i>
                                                                <span className="text-span">{lang.previous}</span>
                                                            </button>
                                                            <a href="#" onClick={this.handleSkip} className="btn btn-link btn-skip mr-30">{lang.skip[0]} <span className="remove-mobile">{lang.skip[1]}</span>{lang.skip[2]}!</a>
                                                        </div>
                                                        <div className="btn-right-side">
                                                            <button onClick={this.handleClickNext} className="btn btn-primary btn-common btn-continue">
                                                                <span className="text-span">{lang.continue}</span>
                                                                <i className="fe fe-arrow-right fe-icon"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AgeGroup;