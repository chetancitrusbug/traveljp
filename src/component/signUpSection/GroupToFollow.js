import React from 'react';
import lang from '../../constant/lang';
import {lanuguage} from '../../config/config';
import { getGroupToFollow, getGroupToFollowSearch } from '../../services/ApiServices';

class GroupToFollow extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            name : this.props.userData.name,
            email : this.props.userData.email,
            occupation : (this.props.userData.occupation !== '' && this.props.userData.occupation !== undefined) ? this.props.userData.occupation : '',
            ageGroup : (this.props.userData.ageGroup !== '' && this.props.userData.ageGroup !== undefined) ? this.props.userData.ageGroup : 0,
            disclose_age: (this.props.userData.disclose_age !== '' && this.props.userData.disclose_age !== undefined) ? this.props.userData.disclose_age : false,
            areaToFollow:(this.props.userData.areaToFollow !== '' && this.props.userData.areaToFollow !== undefined) ? this.props.userData.areaToFollow : [],
            userToFollow:(this.props.userData.userToFollow !== '' && this.props.userData.userToFollow !== undefined) ? this.props.userData.userToFollow : [],
            groupToFollow:(this.props.userData.groupToFollow !== '' && this.props.userData.groupToFollow !== undefined) ? this.props.userData.groupToFollow : [],
            allGroupsToFollow : [],
            search : '',
            password : (this.props.userData.password !== '' && this.props.userData.password !== undefined) ? this.props.userData.password : '',
            whatNext: (this.props.userData.whatNext !== '' && this.props.userData.whatNext !== undefined) ? this.props.userData.whatNext : '',
        }

    }

    async componentDidMount() {
        try {
            const res = await getGroupToFollow();
            const code = res.code;
            switch (code) {
                case 200:
                    this.setState({ allGroupsToFollow: res.data });
                    break;
                case 400:

                    break;

                default:
                    break;
            }
        } catch (error) {

        }
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        
        if(prevProps.allGroupsToFollow !== this.props.allGroupsToFollow){
            return this.props.allGroupsToFollow
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // If we have a snapshot value, we've just added new items.
        // Adjust scroll so these new items don't push the old ones out of view.
        // (snapshot here is the value returned from getSnapshotBeforeUpdate)
        if (snapshot !== null) {
            this.setState({
                allGroupsToFollow:snapshot
            })
        }
    }
    
    
    handleOnChange = (e) => {
        
        if(e.target.checked){
            let groupToFollow = [...this.state.groupToFollow];
            groupToFollow.push(Number(e.target.value));
            this.setState({
                groupToFollow
            })
        }else{
            var groupToFollow = [...this.state.groupToFollow]; // make a separate copy of the array
            var index = groupToFollow.indexOf(Number(e.target.value))
            if (index !== -1) {
                groupToFollow.splice(index, 1);
                this.setState({groupToFollow: groupToFollow});
            }
        }
    }
    
   
    
    handleOnSearch = (e) => {
        this.setState({
            search : e.target.value
        }, async () => {
            try {
                const res = await getGroupToFollowSearch(this.state.search);
                const code = res.code;
                switch (code) {
                    case 200:
                        this.setState({allGroupsToFollow: res.data});
                        break;
                    case 400:
                    
                        break;
                
                    default:
                        break;
                }
            } catch (error) {
    
            }  
        })
    }

    handleSearch = () => {
        // this.props.searchGroup(this.state.search);
    }

    handleSkip = () =>{
        let sections = {
            activeSection : "whatNext" ,
            currentactive : "whatNext"
        }
        this.props.setActiveSection(sections)
    }

    handleClickNext = () => {
        this.props.setUserSignUp({
            name : this.state.name, 
            email:this.state.email,
            occupation : this.state.occupation,
            ageGroup : this.state.ageGroup,
            disclose_age : this.state.disclose_age,
            areaToFollow : this.state.areaToFollow,
            userToFollow : this.state.userToFollow,
            groupToFollow : this.state.groupToFollow,
            password:this.state.password,
            whatNext:this.state.whatNext
        });
        let sections = {
            activeSection : "passwordsection" ,
            currentactive : "passwordsection"
        }
        this.props.setActiveSection(sections)
    }

    handleClickPreviouse = () => {
        this.props.setUserSignUp({
            name : this.state.name, 
            email:this.state.email,
            occupation : this.state.occupation,
            ageGroup : this.state.ageGroup,
            disclose_age : this.state.disclose_age,
            areaToFollow : this.state.areaToFollow,
            userToFollow : this.state.userToFollow,
            groupToFollow : this.state.groupToFollow,
            password:this.state.password,
            whatNext:this.state.whatNext
        });
        let sections = {
            activeSection : "usersToFollow" ,
            currentactive : "usersToFollow"
        }
        this.props.setActiveSection(sections)
    }
    render (){
        let groupToFollow = this.state.groupToFollow;
        let groupLang = lang.groupLang;
        return (
            <div className={"common-div group-div groupToFollow"+ (this.props.currentactive !== 'groupToFollow' ? ' setOpacity' : '')}>
                <div className="container">
                    <div className="row">
    
                        <div className="col-lg-12 col-md-12">        
    
                            <div className="common-card-div">
                                
                                <div className="heading-top-row">
                                    <div className="heading-div"> 
                                        <h2>{groupLang.header}</h2>
                                        <p>{groupLang.subHeading}</p>
                                    </div>
                                </div>

                                <div className="main-body-div">
                                    <div className="search-box-root">
                                        <div className="row get-row mr-minus-8">
                                            <div className="col-lg-12 col-md-12 plr-8">
                                                <div className="input-group input-group-30">
                                                    <input type="text" onChange={this.handleOnSearch} value={this.state.search} className="form-control" placeholder={groupLang.searchPlaceHolder} />
                                                    <div className="input-group-append">
                                                        <button onClick={this.handleSearch} className="btn btn-success btn-search"><i className="material-icons search-icons"> search </i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div className="select-img-root most-img-root">
                                        <div className="row get-row mr-minus-8">
                                            <div className="col-lg-12 col-md-12 plr-8">
                                                <div className="heading-h4"><h4>{groupLang.popularText}</h4></div>
                                            </div>
                                        </div>
                                        <div className={"row get-row mr-minus-8 "}>
                                        { this.state.allGroupsToFollow.map((value,key)=> <div key={key} className="col-lg-3 col-md-3 grid-20 plr-8">
                                            
                                                <div className={"img-checkbox-card-box groups-card-box" +  (groupToFollow.indexOf(value.id) > -1 ? ' selected' : '')  }>
                                                    <label htmlFor={"select-groups-option"+value.id} className="label-box">
                                                        <div className="check-box-position">
                                                            <button className="checkbox-round-div">
                                                                <i className="fe fe-check fe-custom"></i>
                                                            </button>
                                                        </div>
                                                        <div className="img-banner">
                                                        <img src={require(`../../assets/images/groups/${value.image_url}`)} className="img-fluid img-responsive" alt="area" />
                                                        </div>
                                                        <div className="check-title-row">
                                                            <h4>{(lanuguage === 'eng') ? value.name : value.localName}</h4>
                                                        </div>
                                                    </label>
                                                    <input type="checkbox" name="groupToFollow" value={value.id} onChange={this.handleOnChange} className="form-checkbox form-checkbox-groups" id={"select-groups-option"+value.id} />
                                                </div>
                                            </div>
                                        )}
                                           
                                        </div>
                                    </div>
                                </div>

                                {this.props.currentactive === 'groupToFollow' &&
                                    <div className="footer-bottom-row">
                                        <div className="container">
                                            <div className="row mr-minus-8">
                                                <div className="col-lg-12 plr-8">
                                                    <div className="button-row">
                                                        <div className="btn-left-side justify-content-between"> 
                                                            <button onClick={this.handleClickPreviouse}  className="btn btn-outline-primary btn-common btn-previous">
                                                                <i className="fe fe-arrow-left fe-icon"></i>
                                                                <span className="text-span">{lang.previous}</span>
                                                            </button>
                                                            <a href="#" onClick={this.handleSkip} className="btn btn-link btn-skip mr-30">{lang.skip[0]} <span className="remove-mobile">{lang.skip[1]}</span>{lang.skip[2]}!</a>
                                                        </div>
                                                        <div className="btn-right-side"> 
                                                            <button onClick={this.handleClickNext} className="btn btn-primary btn-common btn-continue">
                                                                <span className="text-span">{lang.continue}</span>
                                                                <i className="fe fe-arrow-right fe-icon"></i>
                                                            </button>
                                                        </div>    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>  
                    </div>        
                </div>
            </div> 
        );
    }
}

export default GroupToFollow;