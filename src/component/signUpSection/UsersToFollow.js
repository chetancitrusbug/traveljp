import React from 'react';
import lang from '../../constant/lang';
import { getUserToFollow, getUserToFollowSearch } from '../../services/ApiServices';

class UsersToFollow extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            name : this.props.userData.name,
            email : this.props.userData.email,
            occupation : (this.props.userData.occupation !== '' && this.props.userData.occupation !== undefined) ? this.props.userData.occupation : '',
            ageGroup : (this.props.userData.ageGroup !== '' && this.props.userData.ageGroup !== undefined) ? this.props.userData.ageGroup : 0,
            disclose_age: (this.props.userData.disclose_age !== '' && this.props.userData.disclose_age !== undefined) ? this.props.userData.disclose_age : false,
            areaToFollow:(this.props.userData.areaToFollow !== '' && this.props.userData.areaToFollow !== undefined) ? this.props.userData.areaToFollow : [],
            userToFollow:(this.props.userData.userToFollow !== '' && this.props.userData.userToFollow !== undefined) ? this.props.userData.userToFollow : [],
            allUsersToFollow : [],
            search : '',
            groupToFollow:(this.props.userData.groupToFollow !== '' && this.props.userData.groupToFollow !== undefined) ? this.props.userData.groupToFollow : [],
            password : (this.props.userData.password !== '' && this.props.userData.password !== undefined) ? this.props.userData.password : '',
            whatNext: (this.props.userData.whatNext !== '' && this.props.userData.whatNext !== undefined) ? this.props.userData.whatNext : '',
        }

    }

    async componentDidMount() {
         try {
            const res = await getUserToFollow();
            const code = res.code;
            switch (code) {
                case 200:
                    this.setState({ allUsersToFollow: res.data });
                    break;
                case 400:

                    break;

                default:
                    break;
            }
        } catch (error) {

        }
    }
    
    getSnapshotBeforeUpdate(prevProps, prevState) {
        
        if(prevProps.allUsersToFollow !== this.props.allUsersToFollow){
            return this.props.allUsersToFollow
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // If we have a snapshot value, we've just added new items.
        // Adjust scroll so these new items don't push the old ones out of view.
        // (snapshot here is the value returned from getSnapshotBeforeUpdate)
        if (snapshot !== null) {
            this.setState({
                allUsersToFollow:snapshot
            })
        }
    }
    
    
    handleOnChange = (e) => {
        if(e.target.checked){
            let userToFollow = [...this.state.userToFollow];
            userToFollow.push(Number(e.target.value));
            this.setState({
                userToFollow
            })
        }else{
            var userToFollow = [...this.state.userToFollow]; // make a separate copy of the array
            var index = userToFollow.indexOf(Number(e.target.value))
            if (index !== -1) {
                userToFollow.splice(index, 1);
                this.setState({userToFollow: userToFollow});
            }
        }
    }

    handleOnSearch = (e) => {
        this.setState({
            search : e.target.value
        }, async () => {
            try {
                const res = await getUserToFollowSearch(this.state.search);
                const code = res.code;
                switch (code) {
                    case 200:
                        this.setState({allUsersToFollow: res.data});
                        break;
                    case 400:
                    
                        break;
                
                    default:
                        break;
                }
            } catch (error) {
    
            }  
        });
    }

    handleSearch = () => {
        // this.props.searchUser(this.state.search);
    }

    handleClickNext = () => {
        this.props.setUserSignUp({
            name : this.state.name, 
            email:this.state.email,
            occupation : this.state.occupation,
            ageGroup : this.state.ageGroup,
            disclose_age : this.state.disclose_age,
            areaToFollow : this.state.areaToFollow,
            userToFollow : this.state.userToFollow,
            groupToFollow : this.state.groupToFollow,
            password:this.state.password,
            whatNext:this.state.whatNext
        });
        let sections = {
            activeSection : "groupToFollow" ,
            currentactive : "groupToFollow"
        }
        this.props.setActiveSection(sections)
    }

    handleSkip = () =>{
        let sections = {
            activeSection : "whatNext" ,
            currentactive : "whatNext"
        }
        this.props.setActiveSection(sections)
    }

    handleClickPreviouse = () => {
        this.props.setUserSignUp({
            name : this.state.name, 
            email:this.state.email,
            occupation : this.state.occupation,
            ageGroup : this.state.ageGroup,
            disclose_age : this.state.disclose_age,
            areaToFollow : this.state.areaToFollow,
            userToFollow : this.state.userToFollow,
            groupToFollow : this.state.groupToFollow,
            password:this.state.password,
            whatNext:this.state.whatNext
        });
        let sections = {
            activeSection : "areaToFollow" ,
            currentactive : "areaToFollow"
        }
        this.props.setActiveSection(sections)
    }
    render (){
        let userToFollow = this.state.userToFollow;
        let usersLang = lang.usersLang;
        
        return (
            <div className={"common-div follow-div usersToFollow"+ (this.props.currentactive !== 'usersToFollow' ? ' setOpacity' : '')}>
                <div className="container">
                    <div className="row">
    
                        <div className="col-lg-12 col-md-12">        
    
                            <div className="common-card-div">
                                
                                <div className="heading-top-row">
                                    <div className="heading-div"> 
                                        <h2>{usersLang.header}</h2>
                                        <p>{usersLang.subHeading}</p>
                                    </div>
                                </div>

                                <div className="main-body-div">
                                    
                                    <div className="search-box-root">
                                        <div className="row get-row mr-minus-8">
                                            <div className="col-lg-12 col-md-12 plr-8">
                                                <div className="input-group input-group-30">
                                                    <input type="text" onChange={this.handleOnSearch} value={this.state.search} className="form-control" placeholder={usersLang.searchPlaceHolder} />
                                                    <div className="input-group-append">
                                                        <button onClick={this.handleSearch} className="btn btn-success btn-search"><i className="material-icons search-icons"> search </i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>        

                                    <div className="select-img-root most-img-root popular-user-root">
                                        <div className="row get-row mr-minus-8">
                                            <div className="col-lg-12 col-md-12 plr-8">
                                                <div className="heading-h4"><h4>{usersLang.popularText}</h4></div>
                                            </div>
                                        </div>
                                        <div className="row get-row mr-minus-8">

                                        { this.state.allUsersToFollow.map((value,key)=> <div key={key} className="col-lg-3 col-md-3 grid-20 plr-8">
                                                <div className={"user-card-box follow-card-box" +  (userToFollow.indexOf(value.id) > -1 ? ' selected' : '')  }>
                                                    <label htmlFor={"select-follow-option"+value.id} className="label-box">
                                                        <div className="check-box-position">
                                                            <button className="checkbox-round-div">
                                                                <i className="fe fe-check fe-custom"></i>
                                                            </button>
                                                        </div>
                                                        <div className="img-user-thumb">
                                                             <img src={require(`../../assets/images/user/${value.image_url}`)} className="img-fluid img-responsive" alt="area" />
                                                        </div>
                                                        <div className="check-title-row">
                                                            <h4>{value.name}</h4>
                                                            <p>{value.email}</p>
                                                        </div>
                                                    </label>
                                                    <input type="checkbox" name="userToFollow" onChange={this.handleOnChange} className="form-checkbox form-checkbox-follow" id={"select-follow-option"+value.id}  value={value.id} />
                                                </div>
                                            </div>
                                            )
                                        }
                                        </div>
                                    </div>

                                </div>

                                {this.props.currentactive === 'usersToFollow' &&
                                    <div className="footer-bottom-row">
                                        <div className="container">
                                            <div className="row mr-minus-8">
                                                <div className="col-lg-12 plr-8">
                                                    <div className="button-row">
                                                        <div className="btn-left-side justify-content-between"> 
                                                            <button onClick={this.handleClickPreviouse}  className="btn btn-outline-primary btn-common btn-previous">
                                                                <i className="fe fe-arrow-left fe-icon"></i>
                                                                <span className="text-span">{lang.previous}</span>
                                                            </button>
                                                            <a href="#" onClick={this.handleSkip} className="btn btn-link btn-skip mr-30">{lang.skip[0]} <span className="remove-mobile">{lang.skip[1]}</span>{lang.skip[2]}!</a>
                                                        </div>
                                                        <div className="btn-right-side"> 
                                                            <button onClick={this.handleClickNext} className="btn btn-primary btn-common btn-continue">
                                                                <span className="text-span">{lang.continue}</span>
                                                                <i className="fe fe-arrow-right fe-icon"></i>
                                                            </button>
                                                        </div>    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>  
                    </div>        
                </div>
            </div> 
        );
    }
}

export default UsersToFollow;